CFLAGS += -Wall -Werror -pedantic -std=gnu99

all: sigma_tcp

sigma_tcp: i2c.c regmap.c

clean:
	rm -rf sigma_tcp *.o
